import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.setGlobalPrefix('api');

  const options = new DocumentBuilder()
    .setTitle('Router')
    .setDescription('Documentation')
    .setVersion('1.0')
    .setContact('', '', '')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(process.env.SWAGGER_URI, app, document);
  await app.listen(process.env.PORT);
  logger.log(`Server listen on ${process.env.SERVER_IP}:${process.env.PORT}`);
}
bootstrap();
