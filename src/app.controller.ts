import { Body, Controller, Get, Inject, Param, Post, Res, UploadedFile, UseInterceptors } from "@nestjs/common";
import { AppService } from "./app.service";
import { Client } from "nestjs-soap";
import { RequestRouter, RequestRouter2, RequestRouter3 } from "./dto/RequestRouter";
import { FileInterceptor } from "@nestjs/platform-express";
import { ApiBody, ApiConsumes } from "@nestjs/swagger";
import { fieldName, uploadImage } from "./multer/UploadLoginImageStorage";
import * as fs from "fs";


@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject("PPa") private readonly mySoapClient: Client
  ) {
  }

  @Post("/router")
  async routerController(@Body() request: RequestRouter): Promise<any> {
    return this.appService.routerService(request);
  }

  @Post("/router2")
  async routerController2(
    @Body() request: RequestRouter2,
    @Res() res
  ): Promise<any> {
    for (const key of Object.keys(request.headers)) {
      console.log("key:::" + key);
      console.log("value:::" + request.headers[key]);
      this.mySoapClient.addHttpHeader(key, request.headers[key]);
    }
    const ve = request.version == "1.2" ? "wsPPRequestSoap12" : "wsPPRequestSoap";
    console.log(request.params);
    this.mySoapClient.wsPPRequest[ve][request.action](
      request.params,
      (err, resp) => {
        err && console.error(err);
        try {
          res.send({
            resp: resp,
            err: err
          });
        } catch (e) {
          console.log(e);
          try {
            res.send(resp.data);
          } catch (e) {
            console.log(e);
          }
        }
      }
    );
  }

  @Post("/router3")
  @ApiConsumes("multipart/form-data")
  @UseInterceptors(FileInterceptor(fieldName, { storage: uploadImage }))
  async routerController3(
    @Body() request: RequestRouter3,
    @UploadedFile() file: Express.Multer.File,
    @Res() res
  ): Promise<any> {
    const ve = request.version == "1.2" ? "wsPPRequestSoap12" : "wsPPRequestSoap";
    const heads = JSON.parse(request.headers);
    for (const key of Object.keys(heads)) {
      console.log("key:::" + key);
      console.log("value:::" + heads[key]);
      this.mySoapClient.addHttpHeader(key, heads[key]);
    }
    const params: any = JSON.parse(request.params);
    const fileData = fs.readFileSync(file.path);
    console.log(fileData);
    params.fileName = file.filename;
    params[request.paramsFileFieldName] = fileData;
    this.mySoapClient.wsPPRequest[ve][request.action](
      params,
      (err, resp) => {
        err && console.error(err);
        try {
          res.send({
            resp: resp,
            err: err
          });
        } catch (e) {
          console.log(e);
          try {
            res.send(resp.data);
          } catch (e) {
            console.log(e);
          }
        }
      }
    );
  }

  @Post("/upload-file")
  @ApiConsumes("multipart/form-data")
  @ApiBody({
    schema: {
      type: "object",
      properties: {
        file: {
          type: "string",
          format: "binary"
        }
      }
    }
  })
  @UseInterceptors(FileInterceptor(fieldName, { storage: uploadImage }))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File
  ): Promise<any> {
    return {
      originalName: file.originalname,
      filename: file.filename,
      path: file.path
    };
  }

  @Get("/getActions/:version")
  async getActions(@Param("version") v: string) {
    const ve = v == "1.2" ? "wsPPRequestSoap12" : "wsPPRequestSoap";
    return this.mySoapClient.describe().wsPPRequest[ve];
  }
}
