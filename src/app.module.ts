import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { SoapModule } from 'nestjs-soap';
import * as path from 'path';
import { ServeStaticModule } from "@nestjs/serve-static";
import { MulterModule } from "@nestjs/platform-express";

@Module({
  imports: [
    ConfigModule.forRoot(),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    SoapModule.register({ clientName: 'PPa', uri: process.env.wsdl }),
    ServeStaticModule.forRoot({
      rootPath: path.join(process.cwd(), 'public'),
      serveRoot: `/public`,
    }),
    MulterModule.register({
      dest: path.join(process.cwd(), 'public')
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
