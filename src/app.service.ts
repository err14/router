import { Injectable, Logger } from '@nestjs/common';
import { RequestRouter } from './dto/RequestRouter';
import axios from 'axios';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);

  async routerService(request: RequestRouter): Promise<any> {
    const { method, url, params } = request;
    const headers = {
      ['Content-Type']: 'text/xml; charset=utf-8',
      ...request.headers,
    };
    let xml = '';
    if (request.version === '1.1') {
      xml = `<?xml version="1.0" encoding="utf-8"?>
      <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
      <${request.action} xmlns="https://www.prepaidgate.com/ppRequestc/">
      ${Object.keys(params)
        .map((key, i) => {
          const values = Object.values(params);
          return `<${key}>${values[i]}</${key}>`;
        })
        .join('\n')} 
      </${request.action}>
      </soap:Body>
      </soap:Envelope>`;
    } else if (request.version === '1.2') {
      xml = `<?xml version="1.0" encoding="utf-8"?>
      <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
      <soap12:Body>
      <${request.action} xmlns="https://www.prepaidgate.com/ppRequestc/">
      ${Object.keys(params)
        .map((key, i) => {
          const values = Object.values(params);
          return `<${key}>${values[i]}</${key}>`;
        })
        .join('\n')} 
      </${request.action}>
      </soap12:Body>
      </soap12:Envelope>`;
    }
    let response = null;
    switch (method) {
      case 'POST':
        response = await this.post(url, xml, headers);
    }
    return response;
  }

  /*WITH AXIOS*/
  private async post(url: string, xml: any, headers: any): Promise<any> {
    this.logger.log(
      `${this.post.name} INIT :: ${url}, headers: ${JSON.stringify(headers)}`,
    );
    this.logger.log(`${this.post.name} XML: ${xml}`);
    let response: any;
    try {
      await axios.post(url, xml, { headers }).then((data: any) => {
        response = data.data;
      });
    } catch (e) {
      this.logger.error(`[${this.post.name}] ERROR: ${e}`);
      this.logger.debug(`${this.post.name} DATA: ${e?.response?.data}`);
      response = null;
    }
    return response;
  }

  /*WITH NEST SERVICES*/
  /*private async get(url: string, options?: any): Promise<any> {
    this.logger.log(`[${this.get.name}] HTTP GET :: INIT ${url}, options: ${JSON.stringify(options)}`);
    let response;
    try {
      response = await this.httpService.get(url, options).pipe(
        map(response => response?.data)
      );
    } catch (e) {
      this.logger.error(`[${this.get.name}] ERROR: ${e}`);
      response = null;
    }
    return response;
  }

  private async post(url: string, body: any, options?: any): Promise<any> {
    this.logger.log(`[${this.get.name}] HTTP GET :: INIT ${url}, options: ${JSON.stringify(options)}`);
    let response;
    try {
      response = await this.httpService.post(url, body, options).pipe(
        map(response => response?.data)
      );
    } catch (e) {
      this.logger.error(`[${this.get.name}] ERROR: ${e}`);
      response = null;
    }
    return response;
  }*/
}
