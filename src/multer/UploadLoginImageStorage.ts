import {diskStorage} from "multer";
import * as path from "path";
import {existsSync, mkdirSync} from "fs";
import {v4 as uuid} from 'uuid';

export const fieldName = 'file';
export const uploadImage = diskStorage({
    destination: (req: any, file: any, cb: any) => {
        const uploadPath = path.join(process.cwd(), 'public');
        if (!existsSync(uploadPath)) {
            mkdirSync(uploadPath, {recursive: true});
        }
        cb(null, uploadPath);
    },
    filename(req: any, file: any, cb: any) {
        let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
        cb(null, `${uuid()}${ext}`);
    }
});
