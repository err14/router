import { ApiProperty } from '@nestjs/swagger';

export class RequestRouter {
  @ApiProperty()
  version: string;
  @ApiProperty()
  method: string;
  @ApiProperty()
  url: string;
  @ApiProperty()
  headers: Record<string, any>;
  @ApiProperty()
  params: Record<string, any>;
  @ApiProperty()
  action: string;
}

export class RequestRouter2 {
  @ApiProperty()
  version: string;
  @ApiProperty()
  action: string;
  @ApiProperty()
  headers: Record<string, any>;
  @ApiProperty()
  params: Record<string, any>;
}

export class RequestRouter3 {
  @ApiProperty()
  version: string;
  @ApiProperty()
  action: string;
  @ApiProperty()
  headers: any;
  @ApiProperty()
  params: any;
  @ApiProperty()
  paramsFileFieldName: string;
  @ApiProperty({format: 'binary'})
  file: string
}
